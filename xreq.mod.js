/*******************************************************************************
   Copyright 2018 Repukhov Anton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*******************************************************************************/
;(function(){
  	var object = {
	    SendJSON: function(vector, obj, handler){
	    	var hndl = handler || false;
	    	if(hndl !== false){
	    		//async request
				var xhr = this.GetXM();
				xhr.open('POST', vector, true);
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.onreadystatechange = function(){
					if (xhr.readyState != 4) return;
					if(xhr.status == 200){
						if(xhr.responseText != ''){
							obj = JSON.parse(xhr.responseText);
							handler(obj);
						}else{
							console.log('Empty server response! HNDR: ' + vector);
						}
					}
				}
				xhr.send(JSON.stringify(obj));
			}
			else
			{	//sync request
				var xhr = this.GetXM();
				xhr.open('POST', vector, false);
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.send(JSON.stringify(obj));
				if(xhr.status == 200){
					if(xhr.responseText != ''){
						obj = JSON.parse(xhr.responseText);
						return obj;
					}else{
						console.log('Empty server response! HNDR: ' + vector);
						return false;
					}
				}
			}
	    },
	    GetJSON: function(vector, handler){
	    	var hndl = handler || false;
	    	if(hndl !== false){
	    		//async request
				var xhr = this.GetXM();
				xhr.open('GET', vector, true);
				xhr.onreadystatechange = function(){
					if (xhr.readyState != 4) return;
					if(xhr.status == 200){
						if(xhr.responseText != ''){
							obj = JSON.parse(xhr.responseText);
							handler(obj);
						}else{
							console.log('Empty server response! HNDR: ' + vector);
						}
					}
				}
				xhr.send(null);
			}
			else
			{	//sync request
				var xhr = this.GetXM();
				xhr.open('GET', vector, false);
				xhr.send(null);
				if(xhr.status == 200){
					if(xhr.responseText != ''){
						obj = JSON.parse(xhr.responseText);
						return obj;
					}else{
						console.log('Empty server response! HNDR: ' + vector);
						return false;
					}
				}
			}
	    },
	    GetDATA: function(vector, handler){
	    	var hndl = handler || false;
	    	if(hndl !== false){
	    		//async request
				var xhr = this.GetXM();
				xhr.open('GET', vector, true);
				xhr.onreadystatechange = function(){
					if (xhr.readyState != 4) return;
					if(xhr.status == 200){
						if(xhr.responseText != ''){
							handler(xhr.responseText);
						}else{
							console.log('Empty server response! HNDR: ' + vector);
						}
					}
				}
				xhr.send(null);
			}
			else
			{	//sync request
				var xhr = this.GetXM();
				xhr.open('GET', vector, false);
				xhr.send(null);
				if(xhr.status == 200){
					if(xhr.responseText != ''){
						return xhr.responseText;
					}else{
						console.log('Empty server response! HNDR: ' + vector);
						return false;
					}
				}
			}
	    },
	    SendFORM: function(vector, form, handler){
	    	var hndl = handler || false;
	    	if(hndl !== false){
	    		//async request
				var xhr = this.GetXM();
				xhr.open('POST', vector, true);
				xhr.onreadystatechange = function(){
					if (xhr.readyState != 4) return;
					if(xhr.status == 200){
						if(xhr.responseText != ''){
							data = xhr.responseText;
							handler(data);
						}else{
							console.log('Empty server response! HNDR: ' + vector);
						}
					}
				}
				xhr.send(form);
			}
			else
			{	//sync request
				var xhr = this.GetXM();
				xhr.open('POST', vector, false);
				xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
				xhr.send(form);
				if(xhr.status == 200){
					if(xhr.responseText != ''){
						data = xhr.responseText;
						return data;
					}else{
						console.log('Empty server response! HNDR: ' + vector);
						return false;
					}
				}
			}
	    },
	    GetXM: function(){
			var xmlhttp;
			try{
			    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch(e){
			try{
			    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(E){
			   xmlhttp = false;
			}
			}
			if(!xmlhttp && typeof XMLHttpRequest!='undefined') {
			   xmlhttp = new XMLHttpRequest();
			}
			return xmlhttp;
		}
  	}
  	window.XREQ = object;
})();